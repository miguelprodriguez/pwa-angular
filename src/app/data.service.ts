import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // Create instance
  constructor(private http: HttpClient) { }

  // Method
  gimmeJokes() {
    return this.http.get('https://api.chucknorris.io/jokes/random')
  }

}



