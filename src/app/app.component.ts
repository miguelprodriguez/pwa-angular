import { Component, NgModule } from '@angular/core';
import { SwUpdate } from '@angular/service-worker'

import { DataService } from './data.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'jokes';

  update : boolean = false;
  
  joke: any;
  
  // Instantiate SWUpdate 
  // Instantiate DataService as well
  constructor(updates: SwUpdate, private data: DataService) {
    updates.available.subscribe(event => {
      
      // For soft refresh under the hood of Angular PWA
      updates.activateUpdate().then(() => document.location.reload());

    })

  }

  // When the app loads
  ngOnInit() {
    //data instantiated coming from DataService
    this.data.gimmeJokes().subscribe(res => {
      this.joke = res;
    })
  }

}

